import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from './service/auth/auth.service';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  isLoggedIn: boolean;
  isReady: boolean;
  subscription: Subscription;
  constructor(private authService: AuthService, private router: Router) {
    this.subscription = this.authService.getMessage().subscribe((value) => {
      this.isLoggedIn = value;
    });
  }

  ngOnInit(): void {

    this.authService.isAuthenticated().subscribe((data) => {
      this.isReady = true;
      this.isLoggedIn = data;
      this.router.navigate(['user-page']);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}
