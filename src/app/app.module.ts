import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppComponent} from './app.component';
import {MainPageComponent} from './pages/main-page/main-page.component';
import {UserPageComponent} from './pages/user-page/user-page.component';
import {AdminPageComponent} from './pages/admin-page/admin-page.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginPageComponent} from './pages/login-page/login-page.component';
import {FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {StorageServiceModule} from 'angular-webstorage-service';
import {JwtInterceptor} from './interceptor/jwt-interceptor';
import {AlertModule} from 'ngx-bootstrap';
import {EmployeeToTeamInteractionComponent} from './pages/templates/employee-to-team-interaction/employee-to-team-interaction.component';
import {EmployeeTableComponent} from './pages/templates/employee-table/employee-table.component';
import {UserUpdatePageComponent} from './pages/user-update-page/user-update-page.component';
import {
  MatButtonToggleModule,
  MatCheckboxModule,
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UserPageGuardService} from './service/auth/user-page-guard.service';
import {UserUpdatePageGuardService} from './service/auth/user-update-page-guard.service';
import {LoginPageGuardService} from './service/auth/login-page-guard.service';

const appRoutes: Routes = [
  {path: 'login', component: LoginPageComponent, canActivate: [LoginPageGuardService]},
  {path: 'user-page', component: UserPageComponent, canActivate: [UserPageGuardService]},
  {path: 'user-update-page', component: UserUpdatePageComponent, canActivate: [UserUpdatePageGuardService]},

];

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    UserPageComponent,
    AdminPageComponent,
    LoginPageComponent,
    EmployeeToTeamInteractionComponent,
    EmployeeTableComponent,
    UserUpdatePageComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    StorageServiceModule,
    RouterModule.forRoot(
      appRoutes
    ),
    AlertModule.forRoot(),
    BrowserAnimationsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatButtonToggleModule
  ],
  exports: [
    MatSortModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
    // { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true } can define multiple interceptors
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
