import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

// import 'rxjs/add/operator/do' ;
import {Observable} from 'rxjs';
import {AuthService} from '../service/auth/auth.service';
import {LocalStorageService} from '../service/storage/local-storage.service';
import {JwtService} from '../service/jwt/jwt.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private jwtService: JwtService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const idToken = this.jwtService.getToken();

    if (idToken) {
      const cloned = req.clone({
        headers: req.headers.set(AuthService.AUTHORIZATION_HEADER,
          idToken)

      });

      return next.handle(cloned);
    } else {
      return next.handle(req);
    }
  }
}
