export class EmployeeDto {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  birthday: Date;
  photoPath: string;
  mobileNumber: string;
  hobbies: string;
  phobias: string;
  additionalInfo: string;
  role: string;
}
