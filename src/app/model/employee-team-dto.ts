import {Input} from '@angular/core';

export class EmployeeTeamDto {
  @Input()
  employeeId: number;
  @Input()
  teamId: number;
  @Input()
  isAcceptNotifications: boolean;
  @Input()
  isSendNotifications: boolean;
}
