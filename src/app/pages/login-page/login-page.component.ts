import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LoginRequestDto} from '../../model/login-request-dto';
import {AuthService} from '../../service/auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  isFailedToAuthenticate: boolean;
  authFailureMessage: string;
  @Input()
  public loginRequest: LoginRequestDto;

  @Output()
  public successEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private authService: AuthService, private router: Router) {

  }

  ngOnInit() {
    this.loginRequest = new LoginRequestDto();
  }

  onSubmit() {
    this.authService.login(this.loginRequest)
      .subscribe(data => {
        this.authFailureMessage = null;
        this.successEmitter.emit(true);
          this.authService.saveToken(data.Bearer);
          this.authService.sendMessage(true);
          console.log('successfully authenticated in LDAP' + JSON.stringify(data));
          this.router.navigate(['user-page']);
        }, htpResponseError => {
        this.authFailureMessage = null;
        console.log(JSON.stringify(htpResponseError.error.errorMessage));
          this.authFailureMessage = htpResponseError.error.errorMessage;
          this.isFailedToAuthenticate = true;
        }
      );
  }

}
