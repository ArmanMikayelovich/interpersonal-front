import {AfterViewInit, Component, Input, OnInit, ViewChild} from '@angular/core';
import {EmployeeDto} from '../../../model/employee-dto';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-employee-table',
  templateUrl: './employee-table.component.html',
  styleUrls: ['./employee-table.component.css']
})
export class EmployeeTableComponent implements OnInit, AfterViewInit {
  @Input()
  title: string;

  @Input()
  employees: EmployeeDto[];

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  dataSource: MatTableDataSource<EmployeeDto>;
  displayedColumns: string[] = ['firstName', 'lastName', 'email', 'birthday', 'phoneNumber'];

  constructor() {
  }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<EmployeeDto>(this.employees);
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

}
