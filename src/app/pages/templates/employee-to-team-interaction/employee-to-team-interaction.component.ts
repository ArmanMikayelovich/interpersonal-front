import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TeamDto} from '../../../model/team-dto';
import {EmployeeService} from '../../../service/employee/employee.service';
import {AlertType} from '../../../util/alert-type';

@Component({
  selector: 'app-employee-to-team-interaction',
  templateUrl: './employee-to-team-interaction.component.html',
  styleUrls: ['./employee-to-team-interaction.component.css']
})
export class EmployeeToTeamInteractionComponent implements OnInit {

  @Output()
  public messageEmitter: EventEmitter<any> = new EventEmitter<any>();
  @Input()
  allTeams: TeamDto[];

  @Input()
  employeeTeams: TeamDto[];


  @Output() employeeTeamsChange = new EventEmitter<boolean>();

  constructor(private employeeService: EmployeeService) {
  }

  ngOnInit() {
  }

  teamChanged(value: string) {
    alert(JSON.stringify(value));
  }

  removeEmployeeFromTeam(teamId: number) {
    this.employeeService.removeFromTeam(teamId).subscribe(() => {
      this.employeeTeamsChange.emit(true);
      this.sendMessage(AlertType.success,
        `Successfully left the team ${this.findTeamInArray(teamId).name}!`);
    }, error => {
      alert(JSON.stringify(error));
      this.sendMessage(AlertType.danger,
        `Unable to exit the team ${this.findTeamInArray(teamId).name}, an error occurred!`);
    });

  }

  addEmployeeToTeam(teamId: number) {
    this.employeeService.addToTeam(teamId).subscribe(() => {
      this.sendMessage(AlertType.success,
        `Successfully joined to team ${this.findTeamInArray(teamId).name}!`);
      this.employeeTeamsChange.emit(true);
    }, error => {
      alert(JSON.stringify(error));
      this.sendMessage(AlertType.danger,
        `Unable to join the team ${this.findTeamInArray(teamId).name}, an error occurred!`);
    });
  }

  toNumber(value: string): number {
    return Number.parseInt(value, 10);
  }

  sendMessage(alertType: AlertType, message: string) {
    const messageHolder = {
      alert: alertType,
      msg: message
    };
    this.messageEmitter.emit(messageHolder);
  }

  findTeamInArray(teamId: number) {
    return this.allTeams.find(x => x.id === teamId);
  }

}
