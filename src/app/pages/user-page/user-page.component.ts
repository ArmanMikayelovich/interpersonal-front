import {Component, OnInit} from '@angular/core';
import {JwtService} from '../../service/jwt/jwt.service';
import {EmployeeService} from '../../service/employee/employee.service';
import {DomSanitizer} from '@angular/platform-browser';
import {TeamService} from '../../service/team/team.service';
import {TeamDto} from '../../model/team-dto';
import {EmployeeDto} from '../../model/employee-dto';
import {Observable, ReplaySubject} from 'rxjs';
import {PaginationService} from '../../service/pagination/pagination.service';
import {MatButtonModule} from '@angular/material/button';
@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {
  public isReady = false;
  public teammatesObject: any = {};
  employeeTeams: TeamDto[] = [];
  allTeams: Array<TeamDto> = [];

  constructor(private jwtService: JwtService,
              private employeeService: EmployeeService,
              private teamService: TeamService,
              private domSanitizer: DomSanitizer,
              private paginationService: PaginationService) {
  }

  ngOnInit() {

    this.getTeamsOfEmployee();
    this.getAllTeams();
  }

  getTeammates(teamId: number): Observable<EmployeeDto[]> {
    const emp: ReplaySubject<EmployeeDto[]> = new ReplaySubject<EmployeeDto[]>();
    this
      .employeeService
      .getTeammates(this.jwtService.getUserId(), teamId).subscribe(value => {
      emp.next(value);
    }, error => {
      emp.error(error);
    });
    return emp;

  }

  getAllTeams() {
    this.teamService.getAllTeams().toPromise().then(data => {
      this.allTeams = data;
    }, error => {
      alert(JSON.stringify(error));
      console.log('Error in getAllTeams() - ' + JSON.stringify(error));
    });
  }

  getTeamsOfEmployee() {
    this.teamService.getTeamsOfEmployee(this.jwtService.getUserId())
      .toPromise().then(data => {
      this.employeeTeams = data;
      this.employeeTeams.forEach(team => {
        this.getTeammates(team.id).subscribe(value => {
          this.teammatesObject[team.id] = value;
        });
        this.isReady = true;
      });
      console.log(data);
    }, error => {
      alert(JSON.stringify(error));
      console.log('Error in getTeamsOfEmployee() - ' + JSON.stringify(error));
    });

  }


  // getRoommates(event?) {
  //   if (event) {
  //     this.employeeService.getRoommates(this.jwtService.getUserId(), this.paginationService.getPageWithNumber(event))
  //       .subscribe(data => {
  //         this.roommates = data.content;
  //         this.roommatesPageable = data;
  //       }, error => {
  //         console.log(JSON.stringify(error));
  //         alert(JSON.stringify(error));
  //       });
  //   } else {
  //     this.employeeService.getRoommates(this.jwtService.getUserId(), new PageRequest())
  //       .subscribe(data => {
  //         this.roommates = data.content;
  //         this.roommatesPageable = data;
  //       }, error => {
  //         console.log(JSON.stringify(error));
  //         alert(JSON.stringify(error));
  //       });
  //   }
  // }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

}
