import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {EmployeeDto} from '../../model/employee-dto';
import {EmployeeService} from '../../service/employee/employee.service';
import {JwtService} from '../../service/jwt/jwt.service';
import {TeamDto} from '../../model/team-dto';
import {TeamService} from '../../service/team/team.service';
import {AuthService} from '../../service/auth/auth.service';
import {ActivatedRoute} from '@angular/router';
import {EmployeeTeamDto} from '../../model/employee-team-dto';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';
import {AlertType} from '../../util/alert-type';

@Component({
  selector: 'app-user-update-page',
  templateUrl: './user-update-page.component.html',
  styleUrls: ['./user-update-page.component.css']
})
export class UserUpdatePageComponent implements OnInit, AfterViewInit {
  private _success = new Subject<string>();
  private alertType: string;
  staticAlertClosed = false;
  successMessage: string;

  employeeTeams: TeamDto[] = [];
  allTeams: Array<TeamDto> = [];
  isUserActive: boolean;
  isRequired: boolean;
  updateErrors: any;
  employeeDto: EmployeeDto;
  employeeTeamDtos: any;


  @Input()
  updateRequestDto: EmployeeDto = new EmployeeDto();

  constructor(private employeeService: EmployeeService, private jwtService: JwtService,
              private teamService: TeamService, private authService: AuthService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.getTeamsOfEmployee();
    this.getAllTeams();
    this.checkIsUserActive();
    this.isRequired = history.state.isRequired;
    this.employeeService.getEmployeeById(this.jwtService.getUserId()).subscribe(data => {
      this.employeeDto = data;
    });

    setTimeout(() => this.staticAlertClosed = true, 20000);

    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(2500)
    ).subscribe(() => this.successMessage = null);
  }

  ngAfterViewInit(): void {
    if (this.isRequired === undefined) {
      this.authService.isUserActive().subscribe((data) => {
        this.isRequired = !data;
      });
    }
  }

  public showMessage(alertType: AlertType, message: string) {
    this.alertType = alertType.toLocaleLowerCase();
    this._success.next(message);
  }

  updateProfile(firstName: string, lastName: string, mobileNumber: string, birthday: any, additionalInfo: string) {
    this.updateErrors = {};
    this.updateRequestDto.id = this.jwtService.getUserId();
    this.updateRequestDto.firstName = firstName;
    this.updateRequestDto.lastName = lastName;
    this.updateRequestDto.mobileNumber = mobileNumber;
    this.updateRequestDto.birthday = birthday;
    this.updateRequestDto.additionalInfo = additionalInfo;

    this.employeeService.updateEmployeeProfile(this.updateRequestDto).subscribe(data => {
      console.log(JSON.stringify(data));
      this.authService.saveToken(data.Bearer);
      this.showMessage(AlertType.success,
        this.isUserActive ? 'User information successfully updated.'
          : 'User information successfully updated. Account activated!');
      this.isRequired = false;
      this.isUserActive = true;
    }, error => {
      console.log('Error occurred:' + JSON.stringify(error));
      this.updateErrors = error.error;
      this.showMessage(AlertType.danger, 'Can\'t update user\'s information.');
    });
  }

  getAllTeams() {
    this.teamService.getAllTeams().toPromise().then(data => {
      this.allTeams = data;
    }, error => {
      alert(JSON.stringify(error));
      console.log('Error in getAllTeams() - ' + JSON.stringify(error));
    });
  }

  getTeamsOfEmployee() {
    this.teamService.getTeamsOfEmployee(this.jwtService.getUserId())
      .toPromise().then(data => {
      this.employeeTeams = data;
    }, error => {
      alert(JSON.stringify(error));
      console.log('Error in getTeamsOfEmployee() - ' + JSON.stringify(error));
    });
  }


  checkIsUserActive() {
    this.authService.isUserActive().subscribe(data => {
      this.isUserActive = data;
    }, error => console.log(JSON.stringify(error)));
  }

  updateNotificationsForTeam(teamId: number, sendNotifications: any, getNotifications: any) {
    const employeeTeamDto = new EmployeeTeamDto();
    employeeTeamDto.employeeId = this.jwtService.getUserId();
    employeeTeamDto.teamId = teamId;
    employeeTeamDto.isAcceptNotifications = getNotifications;
    employeeTeamDto.isSendNotifications = sendNotifications;
    this.employeeService.changeTeamNotifications(employeeTeamDto).subscribe(data => {
        console.log(JSON.stringify(data));
        this.showMessage(AlertType.success, 'Information successfully changed');
      },

      error => {
        this.showMessage(AlertType.danger, 'Can\'t update then information');
        console.log('Can\'t update information' + JSON.stringify(error));
      });
  }

}
