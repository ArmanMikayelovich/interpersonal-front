import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TeamDto} from '../../model/team-dto';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private baseUrl = 'http://localhost:8080/api/v1/admin/';


  constructor(private httpClient: HttpClient) {
  }

  addTeam(teamDto: TeamDto): Observable<any> {
    const url = `${this.baseUrl}/add-team`;
    return this.httpClient.post(url, teamDto);
  }

  deleteRoom(roomId: number): Observable<any> {
    const url = `${this.baseUrl}/delete-room`;
    return this.httpClient.delete(url,
      {params: new HttpParams().set('roomId', `${roomId}`)});
  }

  deleteTeam(teamId: number): Observable<any> {
    const url = `${this.baseUrl}/delete-team`;

    return this.httpClient.delete(url,
      {params: new HttpParams().set('teamId', `${teamId}`)});
  }

  changeEmployeeActivation(employeeId: number, isActive: boolean) {
    const url = `${this.baseUrl}/${employeeId}`;
    return this.httpClient.put(url, isActive);
  }

}
