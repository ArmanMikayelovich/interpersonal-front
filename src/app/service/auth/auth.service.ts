import {Injectable} from '@angular/core';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {LoginRequestDto} from '../../model/login-request-dto';
import {JwtService} from '../jwt/jwt.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  static AUTHORIZATION_HEADER = 'Authorization';

  private authMessage = new Subject<any>();


  private baseUrl = 'http://localhost:8080/api/v1/auth/';

  constructor(private httpClient: HttpClient, private jwtService: JwtService) {

  }

  sendMessage(isAuthenticated: boolean) {
    this.authMessage.next({ isAuth : isAuthenticated});
  }

  clearMessages() {
    this.authMessage.next();
  }

  getMessage(): Observable<boolean> {
    return this.authMessage.asObservable();
  }

  login(loginRequest: LoginRequestDto): Observable<any> {
    const url = `${this.baseUrl}login`;
    return this.httpClient.post(url, loginRequest);
  }

  saveToken(token: string) {
    this.jwtService.saveToken(token);
  }

  isAuthenticated(): Observable<boolean> {
    const url = `${this.baseUrl}isAuthenticated`;
    const replaySubject = new ReplaySubject<boolean>();

    this.httpClient.options(url)
      .subscribe(() => replaySubject.next(true), error => replaySubject.next(false));
    return replaySubject;
  }

  isUserActive(): Observable<boolean> {
    const url = `${this.baseUrl}isUserActive`;
    const replaySubject = new ReplaySubject<boolean>();

    this.httpClient.options(url)
      .subscribe(() => replaySubject.next(true), error => replaySubject.next(false));
    return replaySubject;
  }

  logout() {
    this.jwtService.removeToken();
  }


}
