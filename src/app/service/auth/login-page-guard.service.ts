import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {AuthService} from './auth.service';
import {Observable, ReplaySubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginPageGuardService implements CanActivate {


  constructor(public authService: AuthService, public router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const replaySubject = new ReplaySubject<boolean>();

    const isAuthenticatedObservable = this.authService.isAuthenticated();
    isAuthenticatedObservable.subscribe((isAuthenticated) => {
      if (isAuthenticated) {
        this.router.navigate(['user-page']);
        replaySubject.next(false);
        return;
      }
      replaySubject.next(true);
    }, () => replaySubject.next(false));
    return replaySubject;
  }
}
