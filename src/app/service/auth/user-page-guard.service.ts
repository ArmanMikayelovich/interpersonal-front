import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, ReplaySubject} from 'rxjs';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserPageGuardService implements CanActivate {


  constructor(public authService: AuthService, public router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const replaySubject = new ReplaySubject<boolean>();

    const isAuthenticatedObservable = this.authService.isAuthenticated();
    isAuthenticatedObservable.subscribe((isAuthenticated) => {

      if (!isAuthenticated) {
        this.router.navigate(['login']);
        replaySubject.next(isAuthenticated);
        return;
      }

      const isActiveObservable = this.authService.isUserActive();
      isActiveObservable.subscribe((isActive) => {

        if (!isActive) {
          this.router.navigateByUrl('/user-update-page', {state: {isRequired: true}});
        }
        replaySubject.next(isActive);
      }, () => replaySubject.next(false));

    }, () => replaySubject.next(false));
    return replaySubject;
  }
}
