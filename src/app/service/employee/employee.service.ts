import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PageRequest} from '../../util/page-request';
import {Observable} from 'rxjs';
import {PaginationService} from '../pagination/pagination.service';
import {EmployeeDto} from '../../model/employee-dto';
import {EmployeeTeamDto} from '../../model/employee-team-dto';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private baseUrl = 'http://localhost:8080/api/v1/employee/';

  constructor(private httpClient: HttpClient, private paginationService: PaginationService) {
  }

  getEmployeeById(userId: number): Observable<any> {
    return this.httpClient.get(`${this.baseUrl}${userId}`);
  }

  getTeammates(userId: number, teamId: number): Observable<any> {
    const url = `${this.baseUrl}${userId}/getTeammates?teamId=${teamId}`;
    return this.httpClient.get(url);
  }

  addToTeam(teamId: number): Observable<any> {
    const url = `${this.baseUrl}add-to-team?teamId=${teamId}`;
    return this.httpClient.post(url, null);
  }

  changeTeamNotifications(employeeTeamDto: EmployeeTeamDto): Observable<any> {
    const url = `${this.baseUrl}change-team-notice`;
    return this.httpClient.put(url, employeeTeamDto);
  }

  removeFromTeam(teamId: number): Observable<any> {
    const url = `${this.baseUrl}remove-from-team?teamId=${teamId}`;
    return this.httpClient.delete(url);
  }

  searchByName(name: string, pageRequest: PageRequest) {
    const url = `${this.baseUrl}search`;
    const pageWithNumberAndLimit = this.paginationService.getPageWithNumberAndLimit(pageRequest.page, pageRequest.limit);
    const httpParams = this.paginationService.toHttpParam(pageWithNumberAndLimit);
    return this.httpClient.get(url, {params: httpParams});
  }

  updateEmployeeProfile(employeeDto: EmployeeDto): Observable<any> {
    const url = `${this.baseUrl}update`;
    return this.httpClient.post(url, employeeDto);
  }


}
