import {Injectable} from '@angular/core';
import {LocalStorageService} from '../storage/local-storage.service';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class JwtService {
  static TOKEN_NAME = 'Bearer';


  constructor(private storageService: LocalStorageService) {
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);
    if (decoded.exp === undefined) {
      return null;
    }

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if (!token) {
      token = this.getToken();
    }
    if (!token) {
      return true;
    }
    const date = this.getTokenExpirationDate(token);
    if (date === undefined) {
      return false;
    }
    return !(date.valueOf() > new Date().valueOf());
  }

  getToken() {
    return this.storageService.getFromLocal(JwtService.TOKEN_NAME);
  }

  saveToken(token: string) {
    this.storageService.saveInLocal(JwtService.TOKEN_NAME, token);
  }

  getUserId() {
    const token = this.getToken();
    const jwtDecoded = jwt_decode(token);
    return jwtDecoded.id;
  }

  removeToken() {
    this.storageService.saveInLocal(JwtService.TOKEN_NAME, null);
  }
}
