export class Pageable {
  public totalPages: number;
  public totalElements: number;
  public number: number;
}
