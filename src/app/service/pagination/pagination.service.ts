import {Injectable} from '@angular/core';
import {PageRequest} from '../../util/page-request';
import {HttpParams, HttpXsrfTokenExtractor} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaginationService {
  PAGE_PARAMETER = 'page';
  LIMIT_PARAMETER = 'size';

  constructor() {
  }

  /**
   * Standard pagination
   * page number - 0
   * limit 10
   */
  getPagination(): PageRequest {
    return new PageRequest();
  }

  getPageWithNumber(pageNumber?: number): PageRequest {
    const pageRequest = new PageRequest();
    pageRequest.page = pageNumber;
    return pageRequest;
  }


  getPageWithNumberAndLimit(pageNumber: number, limit: number): PageRequest {
    const pageRequest = new PageRequest();
    pageRequest.page = pageNumber;
    pageRequest.limit = limit;
    return pageRequest;
  }

  toHttpParam(page: PageRequest): HttpParams {
    return  new HttpParams()
      .set(this.PAGE_PARAMETER, `${page.page}`)
      .set(this.LIMIT_PARAMETER, `${page.limit}`);
  }

}
