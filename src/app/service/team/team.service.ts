import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {TeamDto} from '../../model/team-dto';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private baseUrl = 'http://localhost:8080/api/v1/teams/';


  constructor(private httpClient: HttpClient) {
  }

  getAllTeams(): Observable<any> {
    return this.httpClient.get(this.baseUrl);
  }

  getTeamsOfEmployee(employeeId: number): Observable<any> {
    return this.httpClient.get(`${this.baseUrl}of-employee/${employeeId}`);
  }

  addTeam(teamDto: TeamDto): Observable<any> {
    return this.httpClient.post(`${this.baseUrl}add-team`, teamDto);
  }

  deleteRoom(teamId: number): Observable<any> {
    return this.httpClient.delete(`${this.baseUrl}delete-team?teamId=${teamId}`);
  }


}
