import {Input} from '@angular/core';

export class PageRequest {
  @Input()
  public page = 0;
  @Input()
  public limit = 10;
}
